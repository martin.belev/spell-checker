const readline = require('readline');
const spellChecker = require('./spell-checker');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function askForWord() {
  return new Promise(resolve => {
    rl.question('Моля въведете дума за проверка: ', word => {
      return resolve(word);
    });
  });
}

async function main() {
  let word = await askForWord();

  while(word) {
    const suggestedWords = spellChecker.getSuggestions(word);
    if (suggestedWords[0] === word) {
      console.log(`Думата '${word}' е правилно написана или не е открита в речника.`);
    } else {
      console.log(`Може би имахте предвид някоя от следните думи: ${suggestedWords.join(', ')}`);
    }

    word = await askForWord();
  }

  rl.close();
}

main();
