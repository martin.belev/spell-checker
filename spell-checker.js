const _ = require('lodash');
const fs = require('fs');

const letters = 'абвгдежзийклмнопрстуфхцчшщъьюя'.split('');
const dictionary = createDictionaryFromText();
const allWordsCount = Object.values(dictionary).reduce((acc, count) => acc + count);

function probability(word) {
  return dictionary[word] / allWordsCount;
}

function getSuggestions(word) {
  let mostProbable = candidates(word.toLowerCase()).map(w => ({word: w, probability: probability(w)}));
  mostProbable = _.orderBy(mostProbable, 'probability', 'desc');
  return mostProbable.slice(0, 3).map(w => w.word);
}

function candidates(word) {
  return known([word]) || known(singleEdit(word)) || known(doubleEdit(word)) || [word];
}

function known(words) {
  const knownWords = _.intersection(words, Object.keys(dictionary));
  return (knownWords.length && knownWords) || null;
}

function singleEdit(word) {
  const length = word.length;
  const deletes = _.range(length).map(i => word.slice(0, i) + word.slice(i + 1));
  const transposes = _.range(length - 1).map(i => word.slice(0, i) + word.slice(i + 1, i + 2) + word.slice(i, i + 1) + word.slice(i + 2));
  const replaces = _.flatten(_.range(length).map(i => letters.map(l => word.slice(0, i) + l + word.slice(i + 1))));
  const inserts = _.flatten(_.range(length + 1).map(i => letters.map(l => word.slice(0, i) + l + word.slice(i))));

  return _.uniq([].concat(deletes, transposes, replaces, inserts));
}

function doubleEdit(word) {
  return _.uniq(_.flatten(singleEdit(word).map(w => singleEdit(w))));
}

function createDictionaryFromText() {
  return _.countBy(fs.readFileSync('./data/bg-text.txt', 'utf8')
    .toString()
    .toLowerCase()
    .match(/[а-я]+/g));
}

module.exports = {
  getSuggestions: getSuggestions
};
